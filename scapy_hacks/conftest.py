import logging
import sys
from contextlib import ExitStack
from pathlib import Path
from typing import Iterator, Optional

import pytest

logger = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def cap_net_raw():
    import pyprctl

    if not pyprctl.Cap.NET_RAW in pyprctl.CapState.get_current().effective:
        pytest.skip("This test requires CAP_NET_RAW")


@pytest.fixture
def exit_stack() -> Iterator[ExitStack]:
    """Return a contextlib.ExitStack as a pytest fixture

    This reduces indentation making code more readable
    """
    with ExitStack() as exit_stack:
        yield exit_stack
