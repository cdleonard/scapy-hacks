import socket
from contextlib import ExitStack
from typing import cast

from scapy.layers.inet import UDP
from scapy.packet import Packet

from .sniffer import SmartCapture, register_packet_match_callback


class UDPLoopbackFixture:
    client_port = 50123
    server_port = 50345
    client_socket: socket.socket
    server_socket: socket.socket

    def __init__(self):
        pass

    def __enter__(self):
        self.client_socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM,
            0,
        )
        self.server_socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM,
            0,
        )
        self.server_socket.bind(("127.0.0.1", self.server_port))
        self.client_socket.bind(("127.0.0.1", self.client_port))
        self.client_socket.connect(("127.0.0.1", self.server_port))
        self.server_socket.connect(("127.0.0.1", self.client_port))
        return self

    def __exit__(self, *a):
        if self.client_socket:
            self.client_socket.close()
        if self.server_socket:
            self.server_socket.close()


def test_udp_loop():
    with UDPLoopbackFixture() as fixture:
        send_data = b"1234"
        fixture.client_socket.send(send_data)
        recv_data = fixture.server_socket.recv(1024)
        assert recv_data == send_data


def test_udp_loop_sniff(exit_stack: ExitStack, cap_net_raw):
    capture = exit_stack.enter_context(
        SmartCapture(
            devname="lo",
            netns=None,
        )
    )
    m = register_packet_match_callback(
        capture,
        lambda p: p if p.haslayer(UDP) else None,
    )
    fixture = exit_stack.enter_context(UDPLoopbackFixture())
    send_data = b"1234"
    assert m.result is None
    fixture.client_socket.send(send_data)
    recv_data = fixture.server_socket.recv(1024)
    assert recv_data == send_data
    m.wait(timeout=1)
    assert m.result is not None
    p = cast(Packet, m.result)
    udp: UDP = p[UDP]
    assert udp.dport == fixture.server_port
    assert udp.sport == fixture.client_port
    assert bytes(udp.payload) == send_data
