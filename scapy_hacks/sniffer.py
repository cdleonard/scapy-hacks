import logging
import threading
import typing
from abc import ABC, abstractmethod
from contextlib import ExitStack
from typing import Any, Callable, Optional

from scapy.packet import Packet
from scapy.sendrecv import AsyncSniffer
from scapy.sessions import DefaultSession

logger = logging.getLogger(__name__)


def create_l2listen_socket_netns(nsname: Optional[str] = None, **kw):
    """Create a scapy L2listen socket inside a namespace"""
    from scapy.config import conf as scapy_conf

    if nsname is None:
        return scapy_conf.L2listen(**kw)
    else:
        from netns import NetNS

        with NetNS(nsname=nsname):
            return scapy_conf.L2listen(**kw)


def scapy_sniffer_start_block(sniffer: AsyncSniffer, timeout=1):
    """Like AsyncSniffer.start except block until sniffing starts

    This ensures no lost packets and no delays
    """
    if sniffer.kwargs.get("started_callback"):
        raise ValueError("sniffer must not already have a started_callback")

    e = threading.Event()
    sniffer.kwargs["started_callback"] = e.set
    sniffer.start()
    e.wait(timeout=timeout)
    if not e.is_set():
        raise TimeoutError("Timed out waiting for sniffer to start")


def scapy_sniffer_stop(sniffer: AsyncSniffer):
    """Like AsyncSniffer.stop except no error is raising if not running"""
    if sniffer is not None and sniffer.running:
        sniffer.stop()


class AsyncSnifferContext(AsyncSniffer):
    def __enter__(self):
        scapy_sniffer_start_block(self)
        return self

    def __exit__(self, *a):
        scapy_sniffer_stop(self)


class PacketHandler(ABC):
    """Interface for receiving a packet

    Can be added to an MultiHandlerSession
    """

    @abstractmethod
    def handle(self, pkt: Packet): ...


class MultiHandlerSession(DefaultSession):
    """A scapy session with a list of `PacketHandler`"""

    handler: typing.List[PacketHandler]

    def __init__(self):
        super().__init__()
        self.handlers = []

    def _call_handlers(self, p: Packet):
        try:
            for handler in self.handlers:
                handler.handle(p)
        except Exception:
            # Print because scapy can otherwise hide such errors
            logger.error("exception on packet %r", p, exc_info=True)
            raise

    # Compat for scapy 2.5.0
    if getattr(DefaultSession, "on_packet_received", None) is not None:

        def on_packet_received(self, p: Optional[Packet]) -> None:
            super().on_packet_received(p)  # type: ignore
            if p is not None:
                self._call_handlers(p)

    else:

        def process(self, p: Packet) -> Packet:
            super().process(p)
            self._call_handlers(p)
            return p


class SmartCapture:
    """Wrapper around scapy capture functionality"""

    sniffer: typing.Optional[AsyncSniffer] = None

    def __init__(
        self,
        netns: Optional[str],
        devname: str,
        print_callback: Optional[Callable[[Packet], Any]] = None,
    ):
        self.netns = netns
        self.devname = devname
        self.session = MultiHandlerSession()
        self.print_callback = print_callback

    def __enter__(self):
        self.exit_stack = ExitStack()
        self.exit_stack.__enter__()
        self.capture_socket = self.exit_stack.enter_context(
            create_l2listen_socket_netns(
                nsname=self.netns,
                iface=self.devname,
            )
        )
        self.sniffer = AsyncSniffer(
            opened_socket=self.capture_socket,
            prn=self.print_callback,
            session=self.session,
        )
        scapy_sniffer_start_block(self.sniffer)
        return self

    def __exit__(self, *args):
        if self.sniffer:
            scapy_sniffer_stop(self.sniffer)
        self.exit_stack.__exit__(*args)


PacketMatchCallback = typing.Callable[[Packet], typing.Any]


class PacketMatcher(PacketHandler):
    """Call a predicate for every packet received and return the first non-null result on .wait"""

    callback: PacketMatchCallback
    event: threading.Event
    result: Any = None

    def __init__(self, callback: PacketMatchCallback):
        self.callback = callback
        self.event = threading.Event()

    def handle(self, p: Packet):
        result = self.callback(p)
        if result is not None:
            self.result = result
            self.event.set()

    def wait(self, timeout=None):
        if self.event.wait(timeout=timeout):
            return self.result
        else:
            raise TimeoutError()


def register_packet_match_callback(
    capture: SmartCapture, callback: PacketMatchCallback
) -> PacketMatcher:
    """Create and register a PacketMatcher"""
    m = PacketMatcher(callback)
    capture.session.handlers.append(m)
    return m
