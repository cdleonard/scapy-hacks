import pytest
from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.packet import Packet
from typing_extensions import assert_type, reveal_type


@pytest.mark.mypy_testing
def test_reveal_type_ether_div_udp() -> None:
    p = Ether() / IP() / UDP()
    reveal_type(p)  # R: scapy.layers.l2.Ether
    assert_type(p, Ether)
    assert isinstance(p, Packet)
    assert isinstance(p, Ether)
