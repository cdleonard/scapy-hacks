import logging

from scapy.layers.inet import UDP

logger = logging.getLogger(__name__)


def test_udp_payload():
    data = bytearray.fromhex("1111222200104444aaaabbbbccccdddd")
    udp = UDP(data)
    assert udp.sport == 0x1111
    assert udp.dport == 0x2222
    assert udp.chksum == 0x4444
    assert len(udp.payload) == 8
    assert bytes(udp.payload)[0] == 0xAA
    assert udp.payload.load[0] == 0xAA
