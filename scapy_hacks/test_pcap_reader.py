from pathlib import Path

import pytest
from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.packet import Packet
from scapy.utils import (
    PcapNgReader,
    PcapReader,
    RawPcapNgReader,
    RawPcapReader,
    wrpcap,
    wrpcapng,
)


def test_alternative():
    assert PcapNgReader.alternative == PcapReader
    assert RawPcapNgReader.alternative == RawPcapReader
    # These are set by metaclass and thus unknown to mypy:
    assert getattr(PcapReader, "alternative") == PcapNgReader
    assert getattr(RawPcapReader, "alternative") == RawPcapNgReader


@pytest.fixture
def tmp_pcap(tmp_path: Path) -> Path:
    pkts: list[Packet]
    pkts = [Ether() / IP() / UDP() for _ in range(10)]
    path = tmp_path / "test1.pcap"
    wrpcap(str(path), pkts)
    return path


@pytest.fixture
def tmp_pcapng(tmp_path: Path) -> Path:
    pkts: list[Packet]
    pkts = [Ether() / IP() / UDP() for _ in range(10)]
    path = tmp_path / "test2.pcap"
    wrpcapng(str(path), pkts)
    return path


def test_read_pcap_type(tmp_pcap: Path, tmp_pcapng: Path):
    """Check PcapReader/PcapNgReader is determined by file data"""
    with PcapReader(str(tmp_pcap)) as reader:
        assert isinstance(reader, PcapReader)
        assert not isinstance(reader, PcapNgReader)
    with PcapNgReader(str(tmp_pcap)) as reader:
        assert isinstance(reader, PcapReader)
        assert not isinstance(reader, PcapNgReader)
    with PcapReader(str(tmp_pcapng)) as reader:
        assert isinstance(reader, PcapReader)
        assert isinstance(reader, PcapNgReader)
    with PcapNgReader(str(tmp_pcapng)) as reader:
        assert isinstance(reader, PcapReader)
        assert isinstance(reader, PcapNgReader)
