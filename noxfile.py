import os

import nox
from nox import Session

python_version_list = ["3.8", "3.9", "3.10", "3.11", "3.12"]
scapy_version_list = ["2.5.0", "master", "custom"]


def get_scapy_install_arg(scapy_param: str) -> str:
    if scapy_param == "master":
        return "git+https://github.com/secdev/scapy"
    elif scapy_param == "custom":
        val = os.getenv("CUSTOM_SCAPY_SOURCE")
        if val is None:
            raise ValueError("Need CUSTOM_SCAPY_SOURCE envvar")
        return val
    else:
        return f"scapy=={scapy_param}"


@nox.session(python=python_version_list)
@nox.parametrize("scapy", scapy_version_list)
def pytest(session: Session, scapy):
    install_args = ["-e", ".[test]"]
    install_args.append(get_scapy_install_arg(scapy))
    session.install(*install_args)
    session.run("pytest", *session.posargs)


@nox.session(python=python_version_list)
@nox.parametrize("scapy", scapy_version_list)
def pytest_mypy(session: Session, scapy):
    install_args = ["-e", ".[test,mypy,pytest-mypy]"]
    install_args.append(get_scapy_install_arg(scapy))
    session.install(*install_args)
    session.run("pytest", *session.posargs)


@nox.session(python=python_version_list)
@nox.parametrize("scapy", scapy_version_list)
def mypy(session: Session, scapy):
    install_args = ["-e", ".[test,mypy]"]
    install_args.append(get_scapy_install_arg(scapy))
    session.install(*install_args)
    session.run("mypy", *session.posargs)
